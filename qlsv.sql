CREATE DATABASE QLSV;
CREATE TABLE DMKHOA (
    MaKH varchar(6) not null,
    TenKhoa varchar(30) not null,
    PRIMARY KEY (MaKH)
);
CREATE TABLE SINHVIEN (
    MaSV varchar(6) not null,
    HoSV varchar(30)not null,
    TenSV varchar(15)not null,
    GioiTinh CHAR(1) not null,
    NgaySinh DATETIME,
    NoiSinh varchar(50) not null,
    DiaChi varchar(50) not null,
    MaKH varchar(6) not null,
    HocBong int,
    PRIMARY KEY (MaSV),
    FOREIGN KEY (MaKH) REFERENCES DMKHOA(MaKH)
);